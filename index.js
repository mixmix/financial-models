const currency = require('currency.js')
const $ = (n) => currency(n, { precision: 10 })

// The following assumes interest is calculated cummulatively at a period less than
// the repayments period

const interest = {
  rate: 4.45, // % per annum
  period: 14 // how often interest is calculated, in days
}
interest.ratePerPeriod = $(interest.rate).divide($(365).divide(interest.period))
interest.multiplierPerPeriod = $(1).add($(interest.ratePerPeriod).divide(100))

const repayment = {
  period: 14, // in days
  // amount: 400 // dollars
  amount: $(354.8) // dollars
}
interest.periodsPerRepayment = repayment.period / interest.period

console.log('Repayment:', Number(repayment.amount))
console.log(`Interest rate per ${interest.period} day(s) ${interest.ratePerPeriod}`)
// console.log({ interest, repayment })

// Qestions
// 1. what are the actual mechanisms for interest?
// 2. can we change the repayment period to daily?
// 3. how to handle float-innacuracies?

// WIP
// This modelling currently predicts 24.1 years for these initial settings
// However their calculator on site estimates 22 years ... (out by 10%)

const state = [
  {
    day: 0,
    amount: $(136610.23),
    interest: $(0),
    principle: $(0)
  }
  // next should be 136488.60
]

while (Number(last(state).amount) > 0) {
  const initial = last(state)
  const next = {
    day: initial.day + repayment.period,
    amount: initial.amount
  }
  for (let i = 0; i < interest.periodsPerRepayment; i++) {
    next.amount = next.amount.multiply(interest.multiplierPerPeriod)
  }

  next.interest = next.amount.subtract(initial.amount)
  next.principle = repayment.amount.subtract(next.interest)
  next.amount = next.amount.subtract(repayment.amount)

  state.push(next)
}

// console.log(JSON.stringify(state, null, 2))
console.log(summary(state))

function summary (state) {
  const initial = {
    years: Math.round(last(state).day / 365 * 10) / 10,
    amount: print$(last(state).amount),
    interest: $(0),
    principle: $(0)
  }
  const result = state.reduce((acc, next) => {
    acc.interest = acc.interest.add(next.interest)
    acc.principle = acc.principle.add(next.principle)

    return acc
  }, initial)

  result.interest = print$(result.interest)
  result.principle = print$(result.principle)
  return result
}

function print$ (curr) {
  const sigFig = 3
  return currency(curr, {
    format (n) {
      const order = Math.round(n).toString().length
      // if (order <= sigFig) return Math.round(n)

      const shift = order - sigFig
      return '$' + Math.round(n * 10 ** -shift) * 10 ** shift
    }
  }).format()
}

function last (arr) {
  return arr[arr.length - 1]
}
